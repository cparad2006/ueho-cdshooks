
const calculaRiesgo=(edad, sexo, imc, tieneDiab1, tieneDiab2, valorHemoglobina,valorHDL, valorCOL, Sistolica, fuma) =>
{

    totalRiesgo = 0;

    totalRiesgo += riesgoEdad(edad, sexo);
    totalRiesgo += riesgoSexo(sexo);
    totalRiesgo += riesgoDiabetesObesidad(imc,tieneDiab1,tieneDiab2,valorHemoglobina);

    /*
    totalRiesgo = totalRiesgo + riesgoHDL(valorHDL, sexo);
    totalRiesgo = totalRiesgo + riesgoCOL(valorCOL, sexo);
    totalRiesgo = totalRiesgo + riesgoSIS(Sistolica, sexo);
    totalRiesgo = totalRiesgo + riesgoDIAB(diab, sexo);
    totalRiesgo = totalRiesgo + riesgoFUMA(fuma, sexo);
     */

    return totalRiesgo;

}
const interpretarRiesgo= (riesgoPaciente)=>
{
    var SuStatus="Riesgo Bajo";
    var SuRecomendacion="Paciente de bajo riesgo si contrae coronavirus";
    var SuIndicador="information";

  if (riesgoPaciente >= 3 && riesgoPaciente <=5)
  {
    var SuStatus="Medio";
    var SuRecomendacion="Paciente de alto Medio si contrae coronavirus";
    var SuIndicador="critical";

  }

  if (riesgoPaciente >= 6)
  {
    var SuStatus="alto";
    var SuRecomendacion="Paciente de alto riesgo si contrae coronavirus";
    var SuIndicador="critical";

  }

    var riesgoData =
    {
    "rtr":
    {
      "status": SuStatus,
      "score": riesgoPaciente,
      "recommendation": SuRecomendacion,
      "indicador":SuIndicador,
      "riesgo":riesgoPaciente
    } }
  ;
    return riesgoData;
}
/*
const riesgoFUMA=(fuma,sexo) =>
{
  var RiesgoFUMA = 0;
  if (fuma)
  {
    if (sexo=='female') {RiesgoFUMA=2}
    if (sexo=='male')   {RiesgoFUMA=2}
  }
  return RiesgoFUMA;
}
const riesgoDIAB=(diab,sexo) =>
{
  var RiesgoDIAB = 0;
  if (diab)
  {
    if (sexo=='female') {RiesgoDIAB=4}
    if (sexo=='male')   {RiesgoDIAB=2}
  }
  return RiesgoDIAB;
}
const riesgoSIS=(valorSIS,sexo) =>
{
  var RiesgoSIS = 0;
  if (valorSIS<120 && valorSIS>0)
  {
    if (sexo=='female') {RiesgoSIS=-3}
    if (sexo=='male')   {RiesgoSIS=-0}
  }
  ;
  if (valorSIS<130 && valorSIS>=120)
  {
    if (sexo=='female') {RiesgoSIS=0}
    if (sexo=='male')   {RiesgoSIS=0}
  }
  if (valorSIS<140 && valorSIS>=130)
  {
    if (sexo=='female') {RiesgoSIS=1}
    if (sexo=='male')   {RiesgoSIS=1}
  }
  if (valorSIS<160 && valorSIS>=140)
  {
    if (sexo=='female') {RiesgoSIS=2}
    if (sexo=='male')   {RiesgoSIS=2}
  }

  if (valorSIS>=160)
  {
    if (sexo=='female') {RiesgoSIS=3}
    if (sexo=='male')   {RiesgoSIS=3}
  }
 return RiesgoSIS;
}

const riesgoCOL=(valorCOL,sexo) =>
{
  var RiesgoCOL = 0;
  if (valorCOL<160 && valorCOL>0)
  {
    if (sexo=='female') {RiesgoCOL=-2}
    if (sexo=='male')   {RiesgoCOL=-3}
  }
  ;
  if (valorCOL<200 && valorCOL>=160)
  {
    if (sexo=='female') {RiesgoCOL=0}
    if (sexo=='male')   {RiesgoCOL=0}
  }
  if (valorCOL<240 && valorCOL>=200)
  {
    if (sexo=='female') {RiesgoCOL=1}
    if (sexo=='male')   {RiesgoCOL=1}
  }
  if (valorCOL<280 && valorCOL>=240)
  {
    if (sexo=='female') {RiesgoCOL=2}
    if (sexo=='male')   {RiesgoCOL=2}
  }

  if (valorCOL>=280)
  {
    if (sexo=='female') {RiesgoCOL=3}
    if (sexo=='male')   {RiesgoCOL=3}
  }
 return RiesgoCOL;
}
const riesgoHDL=(valorHDL,sexo) =>
{
  var RiesgoHDL = 0;
  if (valorHDL<35 && valorHDL>0)
  {
    if (sexo=='female') {RiesgoHDL=5}
    if (sexo=='male')   {RiesgoHDL=2}
  }
  if (valorHDL<45 && valorHDL>=35)
  {
    if (sexo=='female') {RiesgoHDL=2}
    if (sexo=='male')   {RiesgoHDL=1}
  }
  if (valorHDL<50 && valorHDL>=45)
  {
    if (sexo=='female') {RiesgoHDL=1}
    if (sexo=='male')   {RiesgoHDL=0}
  }
  if (valorHDL<60 && valorHDL>=50)
  {
    if (sexo=='female') {RiesgoHDL=0}
    if (sexo=='male')   {RiesgoHDL=0}
  }
  if (valorHDL>=60)
  {
    if (sexo=='female') {RiesgoHDL=-3}
    if (sexo=='male')   {RiesgoHDL=-2}
  }
  return RiesgoHDL;

}
*/

const riesgoDiabetesObesidad = (imc,tieneDiab1,tieneDiab2,valorHemoglobina) => {
  var riesgo = 0;

  if(imc >= 35) {
    riesgo++;
  }

  if(tieneDiab1 || tieneDiab2) {
    if( valorHemoglobina >= 8) { //hemoglobina en porcentaje
      riesgo = riesgo + 2;
    } else {
      riesgo++;
    }
  }
  return riesgo;
}

const riesgoSexo = (sexo) => {
  return sexo == 'male' ? 1 : 0
}

const riesgoEdad = (edad, sexo) =>
{
  var RiesgoPorEdad=0;
  if (edad<= 50)
    {
      RiesgoPorEdad = 0;
    }
    if (edad>50 && edad<=60)
    {
      RiesgoPorEdad = 1;
    }
    if (edad>60 && edad<=70)
    {
      RiesgoPorEdad = 2;
    }

    if (edad>70 && edad<=80)
    {
      RiesgoPorEdad = 4;
    }
    if (edad>80)
    {
      RiesgoPorEdad = 6;
    }

    return RiesgoPorEdad;

}

module.exports={calculaRiesgo,interpretarRiesgo};
