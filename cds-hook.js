const express = require('express');
const moment = require('moment');
const fetch = require('node-fetch');
const asyncHandler = require('express-async-handler')
const bodyParser = require('body-parser');
const cors = require('cors');
const conocimiento = require ('./grundy');
const app = express();

console.log(process.cwd());

// Services
const services = {
  "services": [
    {
      "name": "Demo CDS Hook UEHO",
      "title": "Demo CDS Hook UEHO",
      "id": "ueho-cds-hooks",
      "description": "Calcula riesgo cardíaco de un paciente según Grundy et al, 1999",
      "hook": "patient-view"
    }
  ]
};

// https://www.bma.org.uk/media/3820/bma-covid-19-risk-assessment-tool-february-2021.pdf

const CrearTarjeta = (recoms) =>
{

  if (recoms.rtr === undefined)
    return { "cards": [] };

  var rtrMessage = "El valor de riesgo del paciente si contrae COVID-19 es: " +recoms.rtr.score +" (" + recoms.rtr.status +") "+recoms.rtr.recommendation;
  
  var cards = {
    "cards": [
      {
        "summary": "Herramienta de evaluación de riesgo COVID-19",
        "detail": rtrMessage,
        "source": {
          "label": "https://www.bma.org.uk/media/3820/bma-covid-19-risk-assessment-tool-february-2021.pdf"
        },
        "indicator": recoms.rtr.Indicador
        /*,
        Si quiero puedo agregar sugerencias y acciones, pero no quiero
        "suggestions": [
          

            {
              "summary": "Recomendación habitual para riesgo alto",
              "indicator": "warning",
              "detail": "Se recomienda medicación anticolesterol alto, profilaxis con aspirina (aunque ya no), y control de glucemia y presión arterial.",
              "suggestions": [
                {
                  "label": "Control de Glucemia, Dieta, Control de Colesterol y Presión Arterial",
                }
              ]
            }
          
        ]
        */
      }
    ]
  }

  return (cards);
};

const buildObsURL = (server,patientId, text) => `${server}/Observation?patient=${patientId}&code:text=${text}&_sort:desc=date&_count=1`;
const buildObsLoincURL = (server,patientId, codigo_loinc) => `${server}/Observation?patient=${patientId}&code:code=${codigo_loinc}&_sort:desc=date&_count=1`;
const buildPatientURL = (server,patientId) => `${server}/Patient/${patientId}`;
const buildCondURL = (server,patientId, text) => `${server}/Condition?patient=${patientId}&code:text=${text}&_count=1`;
const buildPesoURL = (server,patientId, text) => `${server}/Condition?patient=${patientId}&code:text=${text}&_count=1`;

const getAsync = async (url) => await (await fetch(url)).json();

app.use(cors());
app.use(bodyParser.json());

app.get('/', asyncHandler(async (req, res, next) => {
  res.send('El servicio CDS Hooks está activo!');
}));

app.get('/cds-services', asyncHandler(async (req, res, next) => {
  res.send(services);
}));

/*
 * Logica del CDS Hook
 */
app.post('/cds-services/ueho-cds-hooks', asyncHandler(async (req, res, next) =>
{
  console.log("CDS Request: \n" + JSON.stringify(req.body, null, ' '));

  // Extracts useful information from the data sent from the Sandbox
  const hook = req.body.hook; // Type of hook
  const fhirServer = req.body.fhirServer; // URL for FHIR Server endpoint
  const patient = req.body.context.patientId; // Patient Identifier

  console.log("Useful parameters:");
  console.log("hook: " + hook);
  console.log("fhirServer: " + fhirServer);
  console.log("patient: " + patient);
  
  const patientReq = await getAsync(buildPatientURL(fhirServer,patient));


  const DiabetesReq=await getAsync(buildCondURL(fhirServer,patient, 'diabetes')); //CONDICION
  const Diabetes1Req=await getAsync(buildCondURL(fhirServer,patient, 'Diabetes mellitus type 1')); //CONDICION
  const Diabetes2Req=await getAsync(buildCondURL(fhirServer,patient, 'Diabetes mellitus type 2')); //CONDICION
  const tieneDiab1 = Diabetes1Req.total > 0;
  const tieneDiab2 = Diabetes2Req.total > 0;
  const diab=(DiabetesReq.total>0);

  //Las demas son Observation (Colesterol, LDL, Presion Sist/Diast, Fumador)
  const HDLChol = await getAsync(buildObsURL(fhirServer,patient,'High Density Lipoprotein Cholesterol')); //OBSERVACION
  const TotChol = await getAsync(buildObsURL(fhirServer,patient,'Total Cholesterol')); //OBSERVACION
  //console.log('url = '+buildObsLoincURL(fhirServer,patient,'LP16413-4'))
  const oHemoglobina = await getAsync(buildObsLoincURL(fhirServer,patient,'4548-4')); //LP16413-4
  console.log(buildObsLoincURL(fhirServer,patient,'4548-4'));
  const Fumador = await getAsync(buildObsURL(fhirServer,patient,'Tobacco Smoking Status NHIS')); //OBSERVACION
  const Presion = await getAsync(buildObsURL(fhirServer,patient,'Blood Pressure')); //OBSERVACION

  const Peso = await getAsync(buildObsURL(fhirServer,patient,'weight')); //OBSERVACION
  const Altura = await getAsync(buildObsURL(fhirServer,patient,'height')); //OBSERVACION

  var peso2 = 0;
  var altura2 = 0;
  var imc = 0;

  if(Peso.entry && Altura.entry) {
    peso2 = parseInt(Peso.entry[0].resource.valueQuantity.value)
    altura2 = parseInt(Altura.entry[0].resource.valueQuantity.value)
    imc = peso2 / ((altura2 / 100) * (altura2 / 100));
  }

 //Seguro que tiene sexo y fecha de nacimiento, calculo edad
  const sexo = patientReq.gender;
  //console.log(patientReq)
  const birthDate = patientReq.birthDate;
  const edad = moment().diff(birthDate, 'years');


  var valorHemoglobina = 0;
  if(oHemoglobina.entry) {
    valorHemoglobina = oHemoglobina.entry[0].resource.valueQuantity.value;
  }

  var valorHDL=0;
  if (HDLChol.entry)
  {
   valorHDL = HDLChol.entry[0].resource.valueQuantity.value;
  }
  var valorCOL=0
  if (TotChol.entry)
  {
    valorCOL=TotChol.entry[0].resource.valueQuantity.value;
  }
  var Sistolica=0;
  
  if (Presion.entry)
  {
    if (Presion.entry[0].resource.component[1])
    {
      Sistolica=Presion.entry[0].resource.component[1].valueQuantity.value;
    }
  }
  /*
  The following smoking status value set of SNOMED CT®  codes, using the preferred concept term, is only required in the context of using the Common Clinical Data Set (CCDS):
  Current every day smoker. 449868002
  Current some day smoker. 428041000124106
  Former smoker. 8517006
  Never smoker. 266919005
  Smoker, current status unknown. 77176002
  Unknown if ever smoked. 266927001
  Heavy tobacco smoker. 428071000124103
  Light tobacco smoker. 428061000124105
*/

  var fuma=false;

  if (Fumador.entry)
  {
    if (Fumador.entry[0].resource.valueCodeableConcept.coding[0].code =="449868002") {fuma=true};
    if (Fumador.entry[0].resource.valueCodeableConcept.coding[0].code =="428041000124106") {fuma=true};
    if (Fumador.entry[0].resource.valueCodeableConcept.coding[0].code =="428071000124103") {fuma=true};
    if (Fumador.entry[0].resource.valueCodeableConcept.coding[0].code =="428061000124105") {fuma=true};
  }



  console.log('Edad: ', edad);
  console.log('Sexo: ', sexo);
  console.log('Diabetes: ', diab);
  console.log('Peso: ', peso2);
  console.log('Altura: ', altura2);
  console.log('IMC: ', imc);
  console.log('Col HDL',valorHDL);
  console.log('Col TOT',valorCOL);
  console.log('Sistolica',Sistolica);
  console.log('Fuma',fuma);
  
  scorePaciente=conocimiento.calculaRiesgo(edad,sexo,imc,tieneDiab1,tieneDiab2,valorHemoglobina,valorHDL,valorCOL,Sistolica,fuma);
  scoreData=conocimiento.interpretarRiesgo(scorePaciente);

   res.send(CrearTarjeta(scoreData));
}));

app.listen(3003, () => console.log('Servicio UEHO CDS Hooks activo en puerto 3003!'))
